#include<stdio.h>
void input_fraction(int *numerator, int *denominator)
{
   printf("enter the numerator");
   scanf("%d",numerator);
   printf("enter the denominator");
   scanf("%d",denominator);
}
void compute(int x1,int y1, int x2, int y2,int *a1, int *b1)
{
    *a1=(x1*y2)+(x2*y1);
    *b1=(y1*y2);
}
void output(int x1,int y1, int x2, int y2,int a1, int b1)
{
     printf("the sum of %d/%d and %d/%d is %d/%d\n",x1,y1,x2,y2,a1,b1);
}
int main()
{
     int x1,x2,y1,y2,a1,b1,sum;
     input_fraction(&x1,&y1);
     input_fraction(&x2,&y2);
     compute(x1,y1,x2,y2,&a1,&b1);
     output(x1,y1,x2,y2,a1,b1);
     return 0; 
}