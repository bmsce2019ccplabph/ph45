#include<stdio.h>
#include<math.h>
int inputx()
{
    int x;
    printf("Enter the xcoordinate of the point\n");
    scanf("%d",&x);
    return x;
}    
int inputy()
{
    int y;
    printf("Enter the ycoordinate of the point\n");
    scanf("%d",&y);
    return y;
}    
int compute(int x1,int x2,int y1,int y2)
{
    float d;
    d=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
    return d;
}
void output(float res)
{
    printf("The distance between the points is %f",res);
}
int main()
{
    int x1,x2,y1,y2;
    float res;
    x1=inputx();
    y1=inputy();
    x2=inputx();
    y2=inputy();
    res=compute(x1,x2,y1,y2);
    output(res);
    return 0;
}    
    